import * as debug from '../debug.js';
import * as utilitiesManager from '../utilities.js';
import * as effectManager from '../effect/effects.js';
import { ARSActor } from '../actor/actor.js';
import { ARSItem } from '../item/item.js';

import { ARS } from '../config.js';

/**
 * Token is a PlaceableObject and we need to manipulate certain views of things for it.
 */

export class ARSToken extends Token {
    // #drawNameplate() {
    //     console.log(`ARSToken: -----------------> we are trying this here.`)
    //     const style = this._getTextStyle();
    //     const name = new PreciseText(this.document.getName(), style);
    //     name.anchor.set(0.5, 0);
    //     name.position.set(this.w / 2, this.h + 2);
    //     return name;
    // }

    // get hasAura() {
    //     if (this.document.actor) {
    //         const auraEffects = this.document.actor.getActiveEffects().filter((effect) => {
    //             return effect.changes.some((changes) => {
    //                 return changes.key.toLowerCase() === 'special.aura';
    //             });
    //         });
    //         return auraEffects;
    //     }
    //     return undefined;
    // }

    get isFriendly() {
        return this.document.disposition == game.ars.const.TOKEN_DISPOSITIONS.FRIENDLY;
    }
    get isNeutral() {
        return this.document.disposition == game.ars.const.TOKEN_DISPOSITIONS.NEUTRAL;
    }
    get isHostile() {
        return this.document.disposition == game.ars.const.TOKEN_DISPOSITIONS.HOSTILE;
    }

    /**@override */
    _onUpdate(data, options, id) {
        // console.log('ARSToken _onUpdate', { data, options, id }, this);
        super._onUpdate(data, options, id);
        if (game.user.id !== id) return;
        this.updateAuras();
    }

    /**@override */
    async draw() {
        // console.log("ARSToken TokenAura draw", { arguments }, this)
        const d = await super.draw(this, arguments);
        this.auraContainer = this.addChildAt(new PIXI.Container(), 0);
        this.updateAuras();
        return d;
    }

    /**
     * Update aura settings
     */
    updateAuras() {
        // console.log("ARSToken TokenAura _updateAuras 1 ===>", this)
        if (this.document?.flags?.world?.aura) {
            const auraConfig = this.document.flags.world.aura;
            if (auraConfig) {
                // this._checkAuras();
                if (this.auraContainer?.removeChildren) {
                    this.auraContainer.removeChildren().forEach((a) => a.destroy());
                }
                if (auraConfig.isEnabled) {
                    let hasPermission = false;
                    if (
                        !auraConfig.permission ||
                        auraConfig.permission === 'all' ||
                        (auraConfig.permission === 'gm' && game.user.isGM)
                    ) {
                        hasPermission = true;
                    } else {
                        hasPermission = !!this.document?.actor?.testUserPermission(
                            game.user,
                            auraConfig.permission.toUpperCase()
                        );
                    }

                    // console.log("ARSToken TokenAura _updateAuras 2 ===>", { auraConfig, hasPermission })
                    if (hasPermission) {
                        const aura = new TokenAura(
                            this,
                            auraConfig.distance,
                            auraConfig.color,
                            auraConfig.isSquare,
                            auraConfig.opacity
                        );
                        this.auraContainer.addChild(aura);
                    }
                }
            } else {
                // console.log("ARSToken TokenAura _updateAuras NO AURACONFIG", this);
            }
        } else {
            // populate defaults some defaults to avoid recreation issues
            if (this.isOwner)
                this.document.setFlag('world', 'aura', {
                    isEnabled: false,
                    color: '#ffffff',
                    opacity: 0.35,
                    distance: 10,
                    permission: 'gm',
                });
        }
        this.updateActiveEffectAura();
    }

    updateActiveEffectAura() {
        if (this?.document?.actor) {
            // get all the aura effects that are active
            const auraEffects = this.document.actor?.getActiveEffects().filter((effect) => {
                return effect.changes.some((changes) => {
                    return changes.key.toLowerCase() === 'special.aura';
                });
            });

            // console.log("token.js updateActiveEffectAura", { auraEffects });

            for (const effect of auraEffects) {
                const auraInfo = effectManager.getAuraInfo(effect, this.document);

                let hasPermission = false;
                const permissionLowerCase = auraInfo?.permission?.toLowerCase();
                switch (permissionLowerCase) {
                    case 'all':
                        {
                            hasPermission = true;
                        }
                        break;

                    // case 'gm': {
                    //     hasPermission = game.user.isGM;
                    // } break;

                    case 'owner':
                        {
                            hasPermission = this.actor?.isOwner;
                        }
                        break;

                    case 'none':
                        {
                            // none one can see
                        }
                        break;
                    default:
                        hasPermission = game.user.isGM; // default is only GM can see
                        break;
                }
                if (auraInfo && hasPermission) {
                    const aura = new TokenAura(this, auraInfo.distance, auraInfo.color, auraInfo.isSquare, auraInfo.opacity);
                    this.auraContainer.addChild(aura);
                }
            }
        } else {
            ui.notifications.error(`Token: ${this.name} does not have an associated actor.`);
        }
    }

    /**
     * DISABLED using Smart-Target mod for now
     *
     * @override so we can make the "targeted by" DOT larger.
     * Going it leave this disabled for now, Smart-Target does the same and GMs can use
     * it or not...
     *
     * Refresh the target indicators for the Token.
     * Draw both target arrows for the primary User and indicator pips for other Users targeting the same Token.
     * @param {ReticuleOptions} [reticule]  Additional parameters to configure how the targeting reticule is drawn.
     * @protected
     */
    // _refreshTarget(reticule) {
    //     this.target.clear();
    //     this.target.removeChildren().forEach(a => a.destroy());

    //     // We don't show the target arrows for a secret token disposition and non-GM users
    //     const isSecret = (this.document.disposition === CONST.TOKEN_DISPOSITIONS.SECRET) && !this.isOwner;
    //     if (!this.targeted.size || isSecret) return;

    //     // Determine whether the current user has target and any other users
    //     const [others, user] = Array.from(this.targeted).partition(u => u === game.user);

    //     // For the current user, draw the target arrows
    //     if (user.length) this._drawTarget(reticule);

    //     // For other users, draw offset pips
    //     const hw = (this.w / 2) + (others.length % 2 === 0 ? 8 : 0);
    //      for (let [i, u] of others.entries()) {
    //         const offset = Math.floor((i + 1) / 2) * 16;
    //         const sign = i % 2 === 0 ? 1 : -1;
    //         const x = hw + (sign * offset);
    //         const tokenImg = u?.character?.img;
    //         this.target.beginFill(Color.from(u.color), 1.0).lineStyle(2, 0x0000000).drawCircle(x, 0, 20);
    //         if (tokenImg) {
    //             const texture = PIXI.Texture.from(tokenImg); // replace 'image_url' with your image's URL

    //             // Create a sprite from the texture
    //             let sprite = new PIXI.Sprite(texture);

    //             // Adjust sprite size if needed
    //             sprite.width = 36; // double the radius
    //             sprite.height = 36; // double the radius

    //             // Set the position
    //             sprite.x = x - sprite.width / 2;  // subtract half the width
    //             sprite.y = 0 - sprite.height / 2; // subtract half the height

    //             // Create a mask
    //             let mask = new PIXI.Graphics();
    //             mask.beginFill(0xffffff);
    //             mask.drawCircle(x, 0, 18);
    //             mask.endFill();

    //             // Assign the mask to the sprite
    //             sprite.mask = mask;

    //             // Then add the sprite and the mask to the stage or any other container
    //             this.target.addChild(sprite);
    //             this.target.addChild(mask);
    //         }
    //     }

    // }
} // end Token

/* -------------------------------------------- */

/**
 * A filter used to apply a reverse mask on the target display object.
 * The caller must choose a channel to use (alpha is a good candidate).
 */
class ReverseMaskFilter extends AdaptiveFragmentChannelMixin(AbstractBaseMaskFilter) {
    /** @override */
    static adaptiveFragmentShader(channel) {
        return `
    precision mediump float;
    varying vec2 vTextureCoord;
    varying vec2 vMaskTextureCoord;
    uniform sampler2D uSampler;
    uniform sampler2D uMaskSampler;
    void main() {
      float mask = 1.0 - texture2D(uMaskSampler, vMaskTextureCoord).${channel};
      gl_FragColor = texture2D(uSampler, vTextureCoord) * mask;
    }`;
    }

    /** @override */
    static defaultUniforms = {
        uMaskSampler: null,
    };
}

/**
 * Token Auras
 */
class TokenAura extends PIXI.Graphics {
    constructor(token, distance, color, isSquare = false, opacity = 0.25) {
        super();
        const squareGrid = canvas.scene.grid.type === 1;
        const dim = canvas.dimensions;
        const unit = dim.size / dim.distance;
        const [cx, cy] = [token.w / 2, token.h / 2];
        const { width, height } = token.document;

        if (distance <= 0) distance = 10;

        // console.log("TokenAura constructor", { squareGrid, dim, unit, cx, cy, width, height })
        let w, h;
        if (isSquare) {
            w = distance * 2 + width * dim.distance;
            h = distance * 2 + height * dim.distance;
        } else {
            [w, h] = [distance, distance];

            if (squareGrid) {
                w += (width * dim.distance) / 2;
                h += (height * dim.distance) / 2;
            } else {
                w += ((width - 1) * dim.distance) / 2;
                h += ((height - 1) * dim.distance) / 2;
            }
        }
        // set w/h
        w *= unit;
        h *= unit;

        this.filters = [this.#createReverseMaskFilter()];

        this.distance = distance;
        this.color = Color.from(color);
        // this.color = color;
        this.isSquare = isSquare;
        // this.source = 'aura';

        this.lineStyle(3, Color.from(color));
        this.beginFill(Color.from(color), opacity);
        if (isSquare) {
            const [x, y] = [cx - w / 2, cy - h / 2];
            this.drawRect(x, y, w, h);
        } else {
            this.drawEllipse(cx, cy, w, h);
        }
        this.endFill();
    }

    // updatePosition(x, y) {
    //     console.log("TokenAura updatePosition", { x, y }, this)
    //     this.position.set(x, y);
    // }

    /**
     * Create the reverse mask filter.
     * @returns {ReverseMaskFilter}  The reference to the reverse mask.
     */
    #createReverseMaskFilter() {
        if (!this.reverseMaskfilter) {
            this.reverseMaskfilter = ReverseMaskFilter.create({
                uMaskSampler: canvas.primary.tokensRenderTexture,
                channel: 'a',
            });
            this.reverseMaskfilter.blendMode = PIXI.BLEND_MODES.NORMAL;
        }
        return this.reverseMaskfilter;
    }
}

/**
 *
 * Extending tokenDocument so that we can filter the TokenConfig
 * dropdown list and block recursion issues
 *
 * and also override name to hide npc names everywhere
 *
 */
export class ARSTokenDocument extends TokenDocument {
    get nameRaw() {
        return this.name;
    }

    /**
     *
     * @returns name processed with some tests
     */
    getName() {
        if (!game.ars.config.settings.identificationActor) return this?.actor?.name || this.name;
        // console.log("token.js getName() ARSTokenDocument", this)
        if (['npc', 'lootable'].includes(this?.actor?.type)) {
            if (!game.user.isGM && !this.actor.isIdentified) {
                return this.actor?.alias ? this.actor.alias : game.i18n.localize('ARS.unknownActor');
            }
        }
        return this.name;
    }

    /** @override */
    async _preCreate(data, options, id) {
        console.log('token.js ARSTokenDocument _preCreate', { data, options, id });
        await super._preCreate(data, options, id);
        if (game.user.isDM && game.ars.config.settings.automateLighting) {
            // set initial light/sight settings checking effects also
            // we have to do this here on creation, otherwise
            // updateSight()/updateLight() will cause errors.
            const light = this?.getLightSettings();
            const sight = this?.getSightSettings();
            await this.updateSource({
                sight: sight,
                light: light,
            });
        }
    }

    /** @override */
    async _onUpdate(data, options, id) {
        // console.log('token.js ARSTokenDocument _onUpdate', { data, options, id });
        await super._onUpdate(data, options, id);
        if (game.user.id !== id) return;

        if (data.hasOwnProperty('x') || data.hasOwnProperty('y') || data.hasOwnProperty('hidden')) {
            if (!game.user.isDM) {
                // refresh ct for vision changes since x/y changed?
                // this is for when other tokens move
                // console.log("hooks.js updateToken data", { data });
                if (game.ars.config.settings.ctShowOnlyVisible) {
                    ui.combat.render();
                }
            }
            if (game.user.isDM) {
                // without the timer the token is mid-move sometimes
                // and aura does not get applied correctly
                const _timeout1 = setTimeout(async () => await this.checkForAuras(), 1000);
                // await this.checkForAuras();
            }
        }
    }
    /** @override */
    async _onCreate(data, options, id) {
        console.log('token.js ARSTokenDocument _onCreate', { data, options, id });
        await super._onCreate(data, options, id);
        if (game.user.isDM) {
            if (this.actor.type === 'npc' && !this.actorLink) {
                await utilitiesManager.postNPCTokenCreate(this);
            }
            await this.checkForAuras();
            await this?.updateSight();
            await this?.updateLight();
        }
    }
    /** @override */
    async _onDelete(options, id) {
        console.log('token.js ARSTokenDocument _onDelete', { options, id });
        await super._onDelete(options, id);
        if (game.user.isDM) {
            //we have to wait for the token to get deleted so we wait 1000
            // const _timeout1 = setTimeout(async () => {
            for (const tokenObj of canvas.tokens?.placeables) {
                tokenObj.document.checkForDanglingAuraEffects();
            }
            // }, 1000);
        }
    }
    prepareDerivedData() {
        super.prepareDerivedData();
        this.name = this.getName();
    }

    // /**@override
    //  *
    //  *
    //  * //BUG: Disabled
    //  * Do this so trim out useless values in the token bar1 and bar2 selection options
    //  *
    //  */
    // static getTrackedAttributes234(data, _path = []) {
    //     // console.log('getTrackedAttributes START', { data, _path });

    //     if (data) {
    //         /**
    //          * we need to filter the data of getTrackedAttributes()
    //          * otherwise it gets in a recursion loop and have stack error
    //          *
    //          * we NEED to ignore "contains" and "containedIn" but the others are simply
    //          * to reduce the spam in the resources drop down for Token config
    //          *
    //          */
    //         // const checkThese = ['attributes', 'abilities', 'details', 'power'];
    //         const ignoreThese = [
    //             'contains',
    //             'containedIn',
    //             'config',
    //             'matrix',
    //             'classes',
    //             'inventory',
    //             'memorizations',
    //             'spellInfo',
    //             'actionInventory',
    //             'gear',
    //             'containers',
    //             'skills',
    //             'weapons',
    //             'spells',
    //             'races',
    //             'rank',
    //             'proficiencies',
    //             'activeClasses',
    //             'armorClass',
    //             'abilityList',
    //             'actionList',
    //             'actions',
    //             'itemActionCount',
    //             'mods',
    //             'actionCount',
    //             'tradeInfo',
    //             'power',
    //         ];

    //         let reducedData = foundry.utils.deepClone(data);
    //         // delete entries we ignore
    //         ignoreThese.forEach((entry) => {
    //             delete reducedData[entry];
    //         });
    //         data = reducedData;
    //     }
    //     // console.log("getTrackedAttributes", { reducedData });
    //     // cleaned up data, now send to the super.
    //     return super.getTrackedAttributes(data, _path);
    // }

    /**
     * Returns the light settings for a token checking effects
     *
     * @returns {Object} Light data
     */
    getLightSettings() {
        // Check if the current user is a game master and if the lighting automation is enabled
        const token = this;

        const light = {
            dim: token.actor.prototypeToken.light.dim || 0,
            bright: token.actor.prototypeToken.light.bright || 0,
            angle: token.actor.prototypeToken.light.angle || 0,
            alpha: token.actor.prototypeToken.light.alpha || 0.5,
            color: token.actor.prototypeToken.light.color || '#FFFFFF',
            luminosity: token.actor.prototypeToken.light.luminosity,
            animation: {
                type: token.actor.prototypeToken.light.animation.type || 0,
            },
        };

        // Loop through each active effect on the token's actor
        for (const effect of token.actor.getActiveEffects()) {
            // Loop through each change in the effect
            for (const change of effect.changes) {
                // If the change key is special.light and has a value
                if (change.key === 'special.light' && change.value.length) {
                    try {
                        const details = JSON.parse(change.value.toLowerCase());
                        const color = ARS.htmlBasicColors[details.color] ? ARS.htmlBasicColors[details.color] : details.color;
                        const dim = parseInt(details.dim) || 0;
                        const bright = parseInt(details.bright) || dim;
                        const angle = parseInt(details.angle) || 360;
                        const animationType = details.animation || null;
                        const alpha = parseFloat(details.alpha) || null;
                        const luminosity = parseFloat(details.luminosity) || 0;

                        // Update light object properties if the conditions are met
                        // if (light.dim < dim || (light.dim === 0 && dim)) {
                        if (dim) {
                            light.dim = dim;
                            light.bright = bright;
                            light.angle = angle;
                            light.color = color;
                            light.luminosity = isNaN(luminosity) ? token.light.luminosity : luminosity;
                            light.animation.type = animationType ?? '';
                            light.alpha = alpha;
                        }
                    } catch (err) {
                        // Show an error notification if the light data field is formatted incorrectly
                        ui.notifications.error(
                            `[${err}] LIGHT data field "${change.value}" is formatted incorrectly. It must be at least "{color: #htmlcolor, dim: lightrange}".`
                        );
                    }
                }
            }
        }
        return light;
    }
    /**
     * Prepare token with light effects or default for NPC/PC
     * Sets light
     */
    async updateLight() {
        // Check if the current user is a game master and if the lighting automation is enabled
        if (game.user.isDM && game.ars.config.settings.automateLighting) {
            const light = this.getLightSettings();
            // Try to update the token's light properties
            try {
                await this.update({ light });
            } catch (err) {
                // Log a warning if the token update for light fails
                console.warn(`${this.name}'s token update for light failed.`);
            }
        }
    }

    getSightSettings() {
        // Get the token object from the current context
        const token = this;
        // Initialize default vision settings
        const sight = {
            enabled: false,
            range: 0,
            sightAngle: 360,
            visionMode: 'basic',
        };
        let detectionModes = token.actor.prototypeToken.detectionModes;

        // Iterate through active effects on the token's actor
        for (const effect of token.actor.getActiveEffects()) {
            // Iterate through effect changes
            for (const change of effect.changes) {
                // Check if the change affects vision and has a value
                if (change.key === 'special.vision' && change.value.length) {
                    // we need some of these to not have case adjusted
                    const details = JSON.parse(change.value);
                    //
                    const detailsLowerCase = JSON.parse(change.value.toLowerCase());

                    try {
                        // Parse change value as JSON
                        const range = parseInt(detailsLowerCase.range);
                        const sightAngle = parseInt(detailsLowerCase.angle) || 360;
                        const visionMode = detailsLowerCase.mode || 'basic';

                        // Update vision settings if the new range is greater or default is 0
                        if (sight.range === 0 || sight.range < range) {
                            sight.enabled = true;
                            sight.range = range;
                            sight.sightAngle = sightAngle;
                            sight.visionMode = visionMode;
                        }
                    } catch (err) {
                        // Show error notification if the change value is not formatted correctly
                        ui.notifications.error(
                            `VISION data field "${change.value}" is formatted incorrectly. It must be at least "sightRange#"`
                        );
                    }

                    const effectDetectionModes = details.detectionModes || undefined;
                    if (effectDetectionModes) {
                        detectionModes = effectDetectionModes;
                    }
                }
            }
        }

        // If no vision changes were found in the active effects
        if (!sight.enabled) {
            // Helper function to set vision settings from the actor's prototype token
            function setSight(actor) {
                sight.enabled = actor.prototypeToken.sight.enabled;
                sight.range = actor.prototypeToken.sight.range;
                sight.sightAngle = actor.prototypeToken.sight.angle;
                sight.visionMode = actor.prototypeToken.sight.visionMode;
            }

            let visionRange = 0;

            // Handle NPC vision settings
            if (token.actor.type === 'npc') {
                const visionCheck = `${token.actor.system?.specialDefenses} ${token.actor.system?.specialAttacks}`;
                try {
                    const [match, ...remain] = visionCheck.match(/vision (\d+)/i) || [];
                    visionRange = match ? parseInt(remain[0]) : 0;

                    if (visionRange) {
                        sight.enabled = true;
                        sight.range = visionRange;
                    } else {
                        setSight(token.actor);
                    }
                } catch (err) {
                    ui.notifications.error(`Error in token.js getSightSettings() ${err}`);
                }
                // Handle character vision settings
            } else if (token.actor.type === 'character') {
                setSight(token.actor);
            }
        }
        return [sight, detectionModes];
    }
    /**
     * Update token vision based on effects applied
     */
    async updateSight() {
        // If the user is not the game master, exit early
        if (game.user.isDM && game.ars.config.settings.automateVision) {
            const [sight, detectionModes] = this.getSightSettings();
            // Check if the actor has the 'blind' status effect
            const blind = this?.actor?.hasStatusEffect('blind');

            // If the actor is not blind, update the token's vision settings
            if (!blind) {
                try {
                    await this.update({ sight, detectionModes });
                    // await this.update({ detectionModes });
                } catch (err) {
                    console.warn(`${this.name}'s token update for vision failed. ${err}`);
                }
            }
        }
    }

    /**
     *
     * Calculate the distance between this token and targetToken
     *
     * @param {TokenDocument} targetToken
     * @returns
     */
    getDistance(targetToken) {
        const tokenA = this.object;
        const tokenB = targetToken.object;
        // Get the grid distance (distance per grid unit)
        const gridDistance = canvas.scene.grid.distance;
        // Get the grid size (pixels per grid unit)
        const gridSize = canvas.scene.grid.size;

        // Calculate the distance in pixels from the center of tokenA to the center of tokenB
        const centerDx = tokenA.x - tokenB.x;
        const centerDy = tokenA.y - tokenB.y;
        const centerDistanceInPixels = Math.sqrt(centerDx * centerDx + centerDy * centerDy);

        // Calculate the minimum distance needed to be added to the centers to measure edge-to-edge distance
        const tokenAEdgeOffset = (tokenA.document.width * gridSize) / 2;
        const tokenBEdgeOffset = (tokenB.document.width * gridSize) / 2;
        const edgeOffset = tokenAEdgeOffset + tokenBEdgeOffset;

        // Subtract the edge offset from the center distance
        const distanceInPixels = Math.max(centerDistanceInPixels - edgeOffset, 0);

        // Convert the distance in pixels to distance in grid units
        const distanceInGridUnits = distanceInPixels / gridSize;

        // Convert the distance in grid units to the actual game distance
        const actualDistance = distanceInGridUnits * gridDistance;

        // console.log(
        //     `${tokenA.name} to ${tokenB.name} = ${actualDistance}`,
        //     { gridDistance, gridSize, distanceInPixels },
        //     Math.round(actualDistance)
        // );
        return Math.round(actualDistance);
    }

    /**
     * Check for aura on token and then any token around it if it needs to apply
     * any changes.* entries on an effect that starts with aura.* will be applied
     * to relevant faction it affects.
     */
    async checkForAuras(hookData = undefined) {
        if (['character', 'npc'].includes(this?.actor?.type)) {
            if (game.ars?.config?.settings?.debugMode)
                console.log('token.js checkForAuras START->', { hookData }, this.name, this);
            for (const tokenObj of canvas.tokens?.placeables) {
                const token = tokenObj.document;
                const allEffects = token.actor?.getActiveEffects();
                if (allEffects) {
                    const auraEffects = allEffects?.filter((effect) => {
                        return effect.changes.some((changes) => {
                            return changes.key.toLowerCase() === 'special.aura';
                        });
                    });

                    // console.log("token.js checkForAuras", { token, actor, allEffects, auraEffects });
                    for (const t of canvas.tokens?.placeables) {
                        for (const effect of auraEffects) {
                            // this doesnt really help, positions need to also be dealt with in checkForDanglingAuraEffects()
                            await this.auraManagement(token, t, effect);
                        }
                    } // canvas
                    // now check to clean any effects left over from deletion/modifications to aura effect
                    await tokenObj.document.checkForDanglingAuraEffects();
                }
            }
        }
    }

    /**
     *
     * Manage aura effects, try and not duplicate, cleanup when missing sources/etc.
     *
     * @param {*} token
     * @param {*} t
     * @param {*} effect
     * @param {*} positions
     * @returns
     */
    async auraManagement(token, t, effect) {
        if (!token || !t) return;
        if (!t.document) return;

        const auraInfo = effectManager.getAuraInfo(effect);
        // self aura
        // TODO: add various tests for affected faction
        // if (auraInfo.faction === 'self') {
        //     if (t.document === token)
        //         activeTarget = true;
        // } else {
        //     activeTarget = false;
        // }
        if (auraInfo) {
            // if token.object.center doesn't exist, the token is probably being/deleted or loaded in another scene
            const tokenDistance = token?.object?.center ? token.getDistance(t.document) : Infinity;

            // check that token disposition matches what aura is suppose to effect and they are in the aura
            const activeTarget = auraInfo.faction === 'all' || t.document.disposition === auraInfo.disposition;

            const auraEnable = activeTarget && tokenDistance <= auraInfo.distance;
            const auraEffectId = utilitiesManager.createHashFromIds(effect.id, token.actor.id); // `${effect.id}-${token.actor.id}`;
            const auraEffect = t.document.actor.effects.get(auraEffectId);
            // console.log('ARSTokenDocument auraManagement', { token, t, effect, auraEnable, auraEffectId, auraEffect });
            if (!auraEnable) return auraEffect?.delete();

            // get any effect that starts with ^aura.* so we can apply them for this aura
            // strip out ^aura. and leave the rest
            const auraChanges = effect.changes
                .filter((c) => c.key.toLowerCase().startsWith('aura.'))
                .map((oC) => {
                    const aC = foundry.utils.duplicate(oC);
                    aC.key = aC.key.toLowerCase()?.replace(/^aura\./, '');
                    return aC;
                });

            //look for a status "effect" entry
            let statusIds = [];
            auraChanges.forEach(async (change, index) => {
                // console.log('token.js auraManagement ', { change, index });
                if (change.key.toLowerCase() === 'special.status' && change.value) {
                    statusIds = statusIds.concat(change.value.split(',').map((text) => text.trim().toLowerCase()) || []);
                }
            });
            const auraEffectData = {
                label: `Within AURA:${effect.name}`,
                img: effect.img,
                origin: `${token.actor.uuid}`,
                'duration.startTime': game.time.worldTime,
                transfer: false,
                changes: auraChanges,
                statuses: statusIds, // make sure we apply statuses as well
                system: {
                    aura: {
                        originUuid: token.actor.uuid,
                        effectUuid: effect.uuid,
                        isAura: true,
                    },
                },
            };

            // console.log('ARSTokenDocument auraManagement', { auraChanges, auraEffectData });

            //TODO: remove this by v13
            // clear old style
            t.document.actor.effects.forEach((e) => {
                if (e.getFlag('world', 'sourceAuraUUID') === effect.uuid) {
                    e.delete();
                }
            });
            //end clear old style

            // if we already have an effect, update
            if (auraEffect) return auraEffect.update(auraEffectData);

            return ActiveEffect.implementation.create(
                { _id: auraEffectId, ...auraEffectData },
                { parent: t.document.actor, keepId: true }
            );
        }
    }

    /**
     *
     * Check for any effects applied from a Aura
     * that are no longer active which can happen when token
     * deleted or moved to another scene.
     *
     */
    async checkForDanglingAuraEffects() {
        if (this?.actor) {
            const auraAppliedEffects = this?.actor?.effects?.filter((e) => {
                return e.system?.aura?.isAura;
            });

            if (auraAppliedEffects) {
                for (const ae of auraAppliedEffects) {
                    const origin = await fromUuid(ae.system.aura.originUuid);
                    const originEffect = await fromUuid(ae.system.aura.effectUuid);
                    const parentSceneId = origin ? origin.getToken()?.object?.scene?.id : undefined;
                    if (
                        !originEffect ||
                        !parentSceneId ||
                        originEffect.disabled ||
                        originEffect.isSuppressed ||
                        parentSceneId != this.object.scene.id
                    ) {
                        ae?.delete();
                    } else {
                        if (['npc', 'character'].includes(origin.type)) {
                            const token = origin.getToken();
                            if (token != this) await this.auraManagement(token, this.object, originEffect);
                        }
                    }
                }
            }
        }
    }
} // end TokenDocument

export class ARSTokenLayer extends TokenLayer {
    /**
     *
     * This override is to push pack versions be dropped into folder called "Map Drops"
     * and to prevent it from making multiple copies there of the same creature (use local if id exists)
     * -cel
     *
     * The bulk of this is copy/paste from foundry.js otherwise
     *
     * @param {*} event
     * @param {*} data
     * @returns
     */
    async _onDropActorData(event, data) {
        // console.log("overrides.js _onDropActorData", { event, data });

        // Ensure the user has permission to drop the actor and create a Token
        if (!game.user.can('TOKEN_CREATE')) {
            return ui.notifications.warn(`You do not have permission to create new Tokens!`);
        }

        //** new code -cel
        let actor = await game.actors.get(data.id);
        //** end new code -cel

        // Acquire dropped data and import the actor
        if (!actor) actor = await Actor.implementation.fromDropData(data);
        if (!actor.isOwner) {
            return ui.notifications.warn(`You do not have permission to create a new Token for the ${actor.name} Actor.`);
        }
        if (actor.compendium) {
            const actorData = game.actors.fromCompendium(actor);
            // actor = await Actor.implementation.create(actorData);
            //** new piece -cel
            const staticId = data.uuid.split('.').pop();
            actor = await Actor.implementation.create({ ...actorData, id: staticId, _id: staticId }, { keepId: true });
            const dumpfolder = await utilitiesManager.getFolder('Map Drops', 'Actor');
            // console.log("overrides.js _onDropActorData", { dumpfolder });
            // actor.update({ 'folder': dumpfolder.id });
            // without the delay see .hud errors
            // const _timeout = setTimeout(() => actor.update({ folder: dumpfolder.id }), 300);
            utilitiesManager.delayExecution(() => actor.update({ folder: dumpfolder.id }), 300);
            //** end new piece -cel
        }

        // Prepare the Token document
        const td = await actor.getTokenDocument(
            {
                hidden: game.user.isGM && event.altKey,
                sort: Math.max(this.getMaxSort() + 1, 0),
            },
            { parent: canvas.scene }
        );

        // Set the position of the Token such that its center point is the drop position before snapping
        const t = this.createObject(td);
        let position = t.getCenterPoint({ x: 0, y: 0 });
        position.x = data.x - position.x;
        position.y = data.y - position.y;
        if (!event.shiftKey) position = t.getSnappedPosition(position);
        t.destroy({ children: true });
        td.updateSource(position);

        // Validate the final position
        if (!canvas.dimensions.rect.contains(td.x, td.y)) return false;

        // Submit the Token creation request and activate the Tokens layer (if not already active)
        this.activate();
        // const cls = getDocumentClass("Token");
        // return cls.create(td, { parent: canvas.scene });
        return td.constructor.create(td, { parent: canvas.scene });
    }
}
