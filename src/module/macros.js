import * as utilitiesManager from './utilities.js';

/* -------------------------------------------- */
/*  Hotbar Macros                               */
/* -------------------------------------------- */

/**
 * Create a Macro from an Item drop.
 *
 * @param {Object} dropData     The dropped data
 * @param {number} slot     The hotbar slot to use
 *
 */
export async function createARSMacro(dropData, slot) {
    console.log('macros.js createARSMacro', { dropData, slot });
    const macroData = { type: 'script', scope: 'actor' };
    switch (dropData.type) {
        case 'Memorization':
            const actor = fromUuidSync(dropData.actorUuid);
            const command =
                `// Macro to use Memorization slot created for "${actor.name}"\n` +
                `const actor = await fromUuid('${dropData.actorUuid}');\n` +
                `const spell = await fromUuid('${dropData.uuid}');\n` +
                `const macroData = {actor:actor,slotIndex:${dropData.macroData.index},slotLevel:${dropData.macroData.level},slotType:'${dropData.macroData.type}'};\n` +
                `spell.runMacro(macroData);\n`;

            foundry.utils.mergeObject(macroData, {
                name: dropData.macroData.name,
                img: dropData.macroData.img,
                // command: `game.ars.rollItemMacro("memorization;${data.data.level};${data.data.index};${data.data.type};${data.data.name}");`,
                command: command,
                flags: { 'ars.itemMacro': true },
            });
            break;

        case 'Item':
            const item = await fromUuid(dropData.uuid);
            if (['weapon', 'skill', 'potion'].includes(item.type)) {
                const command =
                    `// Macro to use Item created for "${item.actor.name}"\n` +
                    `const item = await fromUuid('${item.uuid}');\n` +
                    `item.runMacro();\n`;

                foundry.utils.mergeObject(macroData, {
                    name: item.name,
                    img: item.img,
                    command: command,
                    flags: { 'ars.itemMacro': true },
                });
            }
            break;

        case 'Actor':
        case 'RollTable':
        case 'JournalEntry':
            const objectEntry = await fromUuid(dropData.uuid);
            if (objectEntry) {
                const command =
                    `// Macro to use ${dropData.type}\n` +
                    `const entry = await fromUuid('${objectEntry.uuid}');\n` +
                    `entry.sheet.render(true);\n`;

                foundry.utils.mergeObject(macroData, {
                    name: objectEntry.name,
                    img: objectEntry.img,
                    command: command,
                });
            } else {
                ui.notifications.error(`Cannot find ${dropData.type} to create macro for "${dropData.uuid}".`);
                return true;
            }
            break;

        //allow copy macro to another slot
        case 'Macro':
            const copyMacro = await fromUuid(dropData.uuid);
            if (copyMacro) {
                foundry.utils.mergeObject(macroData, copyMacro.toObject());
            } else {
                ui.notifications.error(`Cannot find macro to copy for "${dropData.uuid}".`);
                return true;
            }
            break;

        case 'Compendium':
            {
                const command = `//Macro to open a Compendium\n` + `game.packs.get("${dropData.id}").render(true)\n`;
                const compendium = game.packs.get(dropData.id);
                if (compendium) {
                    foundry.utils.mergeObject(macroData, {
                        name: compendium.title,
                        img: 'icons/svg/book.svg',
                        command: command,
                    });
                } else {
                    ui.notifications.error(`Cannot find compendium to open "${dropData.id}".`);
                    return true;
                }
            }
            break;

        default:
            ui.notifications.error(`Macros for type "${dropData.type}" are not currently supported.`);
            return true;
            break;
    }

    // console.log("macros.js createARSMacro", { slot, macroData })

    const macro =
        game.macros.find((m) => m.name === macroData.name && m.command === macroData.command && m.author.isSelf) ||
        (await Macro.create(macroData));
    // console.log("macros.js createARSMacro", foundry.utils.duplicate(macro))
    game.user.assignHotbarMacro(macro, slot);
}
