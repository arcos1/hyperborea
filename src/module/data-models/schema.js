const fields = foundry.data.fields;
export class arsBaseDataModel extends foundry.abstract.TypeDataModel {
    // _initializeSource(data, options = {}) {
    //     console.log('arsBaseDataModel _initializeSource', { data, options }, this);
    //     return super._initializeSource(data, options);
    // }

    static defineSchema() {
        return {
            migrate: new fields.BooleanField({
                hint: 'Flag to trigger migration process on next reload',
                label: 'Needs Migration',
                nullable: false,
                required: true,
                initial: false,
                default: false,
                gmOnly: true,
            }),
        };
    }
}

/**
 * look into "choices" option on StringField({choices: {a: 'a',b: 'b'}}) ??
 */

/**
 * Currency data schema used in actors and items
 */
export class currencyDataSchema extends arsBaseDataModel {
    static defineSchema() {
        return {
            currency: new fields.SchemaField({
                // using string because we can have formulas in
                // these for npcs not placed on map
                pp: new fields.StringField({ default: 0 }),
                gp: new fields.StringField({ default: 0 }),
                ep: new fields.StringField({ default: 0 }),
                sp: new fields.StringField({ default: 0 }),
                cp: new fields.StringField({ default: 0 }),
            }),
        };
    }
}
