import { arsBaseDataModel, currencyDataSchema } from '../schema.js';

const fields = foundry.data.fields;

// function addSpellSlots(count) {
//     const slots = [];
//     for (let i = 0; i <= count; i++) {
//         // slots.push(new fields.ObjectField({ required: true }));
//         slots.push(new fields.ObjectField({}));
//     }
//     return slots;
// }

/**
 * Base Actor schema
 */
export class actorDataSchema extends arsBaseDataModel {
    // /** @inheritdoc */
    static migrateData(source) {
        // console.log('source---', { source });
        if (source.attributes && source.attributes.movement && !Number.isFinite(source.attributes.movement.value)) {
            console.log('actorDataSchema migrateData movement.value', { source }, source.attributes.movement.value);
            source.attributes.movement.text = String(source.attributes.movement.value);
            source.attributes.movement.value = Number(source.attributes.movement.value) || 0;
            source.migrate = true;
        }

        // // check that memorization slots are not mucked up
        // if (source?.spellInfo?.memorization) {
        //     /**
        //      *
        //      * @param {Object} arcane/divine slot objects
        //      * @param {Number} 9 or 7
        //      */
        //     function ensureKeys(type, obj, range) {
        //         console.log(`>>>>>>>>>>>checking ${type}`, range, obj, source);
        //         for (let num = 0; num <= range; num++) {
        //             if (!(num in obj)) {
        //                 console.warn(`${type} missing slot ${num} as =${obj[num]}`, { obj, source });
        //                 obj[num] = {}; // or some default value
        //                 // source.migrate = true;
        //             }
        //             source.spellInfo.memorization[`${type}Test`][num] = Object.values(obj[num]);
        //         }
        //     }

        //     const limits = { arcane: 9, divine: 7 };
        //     const properties = Object.keys(limits);

        //     properties.forEach((prop) => {
        //         source.spellInfo.memorization[prop] = source.spellInfo.memorization[prop] || {};
        //         ensureKeys(prop, source.spellInfo.memorization[prop], limits[prop]);
        //     });
        // }
    }

    static defineSchema() {
        return {
            // insert the schema bits from our base
            ...super.defineSchema(),
            abilities: new fields.SchemaField({
                str: new fields.SchemaField({
                    //TODO: Eventually... base on abilities/saves will be used as "base" value to more easily deal with effects/etc
                    // not used yet
                    base: new fields.NumberField({ require: true, initial: 0, default: 10 }),
                    //

                    value: new fields.NumberField({ require: true, initial: 9, default: 10 }),
                    percent: new fields.NumberField({ require: true, initial: 0, default: 0 }),
                }),
                dex: new fields.SchemaField({
                    base: new fields.NumberField({ require: true, initial: 0, default: 10 }),
                    value: new fields.NumberField({ require: true, initial: 9, default: 10 }),
                    percent: new fields.NumberField({ require: true, initial: 0, default: 0 }),
                }),
                con: new fields.SchemaField({
                    base: new fields.NumberField({ require: true, initial: 0, default: 10 }),
                    value: new fields.NumberField({ require: true, initial: 9, default: 10 }),
                    percent: new fields.NumberField({ require: true, initial: 0, default: 0 }),
                }),
                int: new fields.SchemaField({
                    base: new fields.NumberField({ require: true, initial: 0, default: 10 }),
                    value: new fields.NumberField({ require: true, initial: 9, default: 10 }),
                    percent: new fields.NumberField({ require: true, initial: 0, default: 0 }),
                }),
                wis: new fields.SchemaField({
                    base: new fields.NumberField({ require: true, initial: 0, default: 10 }),
                    value: new fields.NumberField({ require: true, initial: 9, default: 10 }),
                    percent: new fields.NumberField({ require: true, initial: 0, default: 0 }),
                }),
                cha: new fields.SchemaField({
                    base: new fields.NumberField({ require: true, initial: 0, default: 10 }),
                    value: new fields.NumberField({ require: true, initial: 9, default: 10 }),
                    percent: new fields.NumberField({ require: true, initial: 0, default: 0 }),
                }),
            }),
            saves: new fields.SchemaField({
                paralyzation: new fields.SchemaField({
                    base: new fields.NumberField({ require: true, initial: 0, default: 20 }),
                    value: new fields.NumberField({ required: true, initial: 20, default: 20 }),
                    label: new fields.StringField({ default: 'Needs Label' }),
                }),
                poison: new fields.SchemaField({
                    base: new fields.NumberField({ require: true, initial: 0, default: 20 }),
                    value: new fields.NumberField({ required: true, initial: 20, default: 20 }),
                    label: new fields.StringField({ default: 'Needs Label' }),
                }),
                death: new fields.SchemaField({
                    base: new fields.NumberField({ require: true, initial: 0, default: 20 }),
                    value: new fields.NumberField({ required: true, initial: 20, default: 20 }),
                    label: new fields.StringField({ default: 'Needs Label' }),
                }),
                rod: new fields.SchemaField({
                    base: new fields.NumberField({ require: true, initial: 0, default: 20 }),
                    value: new fields.NumberField({ required: true, initial: 20, default: 20 }),
                    label: new fields.StringField({ default: 'Needs Label' }),
                }),
                staff: new fields.SchemaField({
                    base: new fields.NumberField({ require: true, initial: 0, default: 20 }),
                    value: new fields.NumberField({ required: true, initial: 20, default: 20 }),
                    label: new fields.StringField({ default: 'Needs Label' }),
                }),
                wand: new fields.SchemaField({
                    base: new fields.NumberField({ require: true, initial: 0, default: 20 }),
                    value: new fields.NumberField({ required: true, initial: 20, default: 20 }),
                    label: new fields.StringField({ default: 'Needs Label' }),
                }),
                petrification: new fields.SchemaField({
                    base: new fields.NumberField({ require: true, initial: 0, default: 20 }),
                    value: new fields.NumberField({ required: true, initial: 20, default: 20 }),
                    label: new fields.StringField({ default: 'Needs Label' }),
                }),
                polymorph: new fields.SchemaField({
                    base: new fields.NumberField({ require: true, initial: 0, default: 20 }),
                    value: new fields.NumberField({ required: true, initial: 20, default: 20 }),
                    label: new fields.StringField({ default: 'Needs Label' }),
                }),
                breath: new fields.SchemaField({
                    base: new fields.NumberField({ require: true, initial: 0, default: 20 }),
                    value: new fields.NumberField({ required: true, initial: 20, default: 20 }),
                    label: new fields.StringField({ default: 'Needs Label' }),
                }),
                spell: new fields.SchemaField({
                    base: new fields.NumberField({ require: true, initial: 0, default: 20 }),
                    value: new fields.NumberField({ required: true, initial: 20, default: 20 }),
                    label: new fields.StringField({ default: 'Needs Label' }),
                }),
            }),
            spellInfo: new fields.SchemaField({
                level: new fields.SchemaField({
                    arcane: new fields.SchemaField({
                        value: new fields.NumberField({ default: 0 }),
                        bonus: new fields.NumberField({ default: 0 }),
                    }),
                    divine: new fields.SchemaField({
                        value: new fields.NumberField({ default: 0 }),
                        bonus: new fields.NumberField({ default: 0 }),
                    }),
                }),
                memorization: new fields.SchemaField({
                    // arcaneTest: new fields.ArrayField(new fields.ObjectField({ required: true }), {
                    //     initial: () => Array(10).fill({}), //() => Array.from({ length: 10 }, () => ({})),,
                    //     min: 10,
                    //     max: 10,
                    // }),
                    // divineTest: new fields.ArrayField(new fields.ObjectField({ required: true }), {
                    //     initial: () => Array(8).fill({}),
                    //     min: 10,
                    //     max: 10,
                    // }),
                    // arcaneTest2: new fields.SchemaField(new fields.ObjectField({ required: true }), {
                    //     initial: () => Object.fromEntries(Array.from(Array(10).fill({}), (_, i) => [i, {}])),
                    // }),
                    // arcaneTest: new fields.ArrayField(new fields.ObjectField({ required: true }), {
                    //     validate: (c) => c.length === 10,
                    //     validationError: 'must be a length-10 array of integer coordinates',
                    // }),
                    // divineTest: new fields.ArrayField(new fields.ObjectField({ required: true }), {
                    //     validate: (c) => c.length === 8,
                    //     validationError: 'must be a length-8 array of integer coordinates',
                    // }),
                    // divineTest: new fields.ArrayField(addSpellSlots(7)),
                    arcane: new fields.SchemaField({
                        0: new fields.ObjectField({ required: true }),
                        1: new fields.ObjectField({ required: true }),
                        2: new fields.ObjectField({ required: true }),
                        3: new fields.ObjectField({ required: true }),
                        4: new fields.ObjectField({ required: true }),
                        5: new fields.ObjectField({ required: true }),
                        6: new fields.ObjectField({ required: true }),
                        7: new fields.ObjectField({ required: true }),
                        8: new fields.ObjectField({ required: true }),
                        9: new fields.ObjectField({ required: true }),
                    }),
                    divine: new fields.SchemaField({
                        0: new fields.ObjectField({ required: true }),
                        1: new fields.ObjectField({ required: true }),
                        2: new fields.ObjectField({ required: true }),
                        3: new fields.ObjectField({ required: true }),
                        4: new fields.ObjectField({ required: true }),
                        5: new fields.ObjectField({ required: true }),
                        6: new fields.ObjectField({ required: true }),
                        7: new fields.ObjectField({ required: true }),
                    }),
                }),
                slots: new fields.SchemaField({
                    arcane: new fields.SchemaField({
                        value: new fields.SchemaField({
                            0: new fields.NumberField({ required: true, initial: 0, default: 0 }),
                            1: new fields.NumberField({ required: true, initial: 0, default: 0 }),
                            2: new fields.NumberField({ required: true, initial: 0, default: 0 }),
                            3: new fields.NumberField({ required: true, initial: 0, default: 0 }),
                            4: new fields.NumberField({ required: true, initial: 0, default: 0 }),
                            5: new fields.NumberField({ initial: 0, default: 0 }),
                            6: new fields.NumberField({ initial: 0, default: 0 }),
                            7: new fields.NumberField({ initial: 0, default: 0 }),
                            8: new fields.NumberField({ initial: 0, default: 0 }),
                            9: new fields.NumberField({ initial: 0, default: 0 }),
                        }),
                        bonus: new fields.ArrayField(new fields.NumberField({ default: 0 })),
                    }),
                    divine: new fields.SchemaField({
                        value: new fields.SchemaField({
                            0: new fields.NumberField({ initial: 0, default: 0 }),
                            1: new fields.NumberField({ initial: 0, default: 0 }),
                            2: new fields.NumberField({ initial: 0, default: 0 }),
                            3: new fields.NumberField({ initial: 0, default: 0 }),
                            4: new fields.NumberField({ initial: 0, default: 0 }),
                            5: new fields.NumberField({ initial: 0, default: 0 }),
                            6: new fields.NumberField({ initial: 0, default: 0 }),
                            7: new fields.NumberField({ initial: 0, default: 0 }),
                        }),
                        bonus: new fields.ArrayField(new fields.NumberField({ default: 0 })),
                    }),
                }),
            }),
            attributes: new fields.SchemaField({
                ac: new fields.SchemaField({ value: new fields.NumberField({ initial: 10, default: 10 }) }),
                thaco: new fields.SchemaField({ value: new fields.NumberField({ initial: 20, default: 20 }) }),
                hp: new fields.SchemaField({
                    base: new fields.NumberField({ required: true, initial: 0, default: 0 }),
                    value: new fields.NumberField({ required: true, initial: 0, default: 0 }),
                    min: new fields.NumberField({ required: true, initial: -10, default: -10 }),
                    max: new fields.NumberField({ required: true, initial: 0, default: 0 }),
                    // percent: new fields.NumberField({ required: true, initial: 0, default: 0 }),
                    temp: new fields.NumberField({ required: true, initial: 0, default: 0 }),
                    tempmax: new fields.NumberField({ required: true, initial: 0, default: 0 }),
                }),
                init: new fields.SchemaField({
                    value: new fields.NumberField({ default: 0 }),
                    modifier: new fields.NumberField({ default: 0 }),
                }),
                movement: new fields.SchemaField({
                    // migrate value to integer
                    value: new fields.NumberField({ initial: 0, default: 0 }),
                    text: new fields.StringField({ initial: '', default: '' }),
                    unit: new fields.StringField({ default: 'ft' }),
                }),
                size: new fields.StringField({ initial: 'medium', default: 'medium' }),
                identified: new fields.BooleanField({ default: false }),
            }),
            details: new fields.SchemaField({
                biography: new fields.SchemaField({
                    value: new fields.StringField({ default: '' }),
                    public: new fields.StringField({ default: '' }),
                    source: new fields.StringField({ trim: true, default: '' }),
                }),
                provisions: new fields.SchemaField({
                    food: new fields.StringField({ blank: true, nullable: true, initial: '', default: '' }),
                    water: new fields.StringField({ blank: true, nullable: true, initial: '', default: '' }),
                }),
                alignment: new fields.StringField({ initial: 'n', trim: true, default: '' }),

                // //npc specific
                // type: new fields.StringField({ trim: true, default: '' }),
                // source: new fields.StringField({ trim: true, default: '' }),

                // // pc specific
                // age: new fields.StringField({ default: '' }),
                // sex: new fields.StringField({ default: '' }),
                // height: new fields.StringField({ default: '' }),
                // weight: new fields.StringField({ default: '' }),
                // deity: new fields.StringField({ default: '' }),
                // patron: new fields.StringField({ default: '' }),
                // background: new fields.StringField({ default: '' }),
                // notes: new fields.StringField({ default: '' }),
                // xp: new fields.NumberField({ default: 0 }),
                // applyxp: new fields.NumberField({ default: 0 }),
            }),
            ...currencyDataSchema.defineSchema(),
            // available spell slots for memorizations
            memorizations: new fields.SchemaField({
                arcane: new fields.SchemaField({
                    0: new fields.ObjectField({ required: true }),
                    1: new fields.ObjectField({ required: true }),
                    2: new fields.ObjectField({ required: true }),
                    3: new fields.ObjectField({ required: true }),
                    4: new fields.ObjectField({ required: true }),
                    5: new fields.ObjectField({ required: true }),
                    6: new fields.ObjectField({ required: true }),
                    7: new fields.ObjectField({ required: true }),
                    8: new fields.ObjectField({ required: true }),
                    9: new fields.ObjectField({ required: true }),
                }),
                divine: new fields.SchemaField({
                    0: new fields.ObjectField({ required: true }),
                    1: new fields.ObjectField({ required: true }),
                    2: new fields.ObjectField({ required: true }),
                    3: new fields.ObjectField({ required: true }),
                    4: new fields.ObjectField({ required: true }),
                    5: new fields.ObjectField({ required: true }),
                    6: new fields.ObjectField({ required: true }),
                    7: new fields.ObjectField({ required: true }),
                }),
            }),
            psionics: new fields.SchemaField({
                mthaco: new fields.SchemaField({
                    value: new fields.NumberField({ require: true, initial: 0, default: 0 }),
                    base: new fields.NumberField({ require: true, initial: 0, default: 0 }),
                }),
                mac: new fields.SchemaField({
                    base: new fields.NumberField({ require: true, initial: 0, default: 0 }),
                    value: new fields.NumberField({ require: true, initial: 0, default: 0 }),
                }),
                psp: new fields.SchemaField({
                    base: new fields.NumberField({ require: true, initial: 0, default: 0 }),
                    value: new fields.NumberField({ require: true, initial: 0, default: 0 }),
                    min: new fields.NumberField({ require: true, initial: 0, default: 0 }),
                    max: new fields.NumberField({ require: true, initial: 0, default: 0 }),
                }),
            }),

            // generic properties used for various things.
            properties: new fields.ArrayField(new fields.StringField()),
            // mods data from effects/etc
            mods: new fields.ObjectField(),
            // combat matrix for v0/1
            matrix: new fields.ArrayField(new fields.ObjectField()),
            // current ranks/levels
            rank: new fields.ObjectField(),
            // data points for trading between players with request trade
            tradeInfo: new fields.ObjectField(),
            // old actions, can be removed in v13
            actions: new fields.ArrayField(new fields.ObjectField()),
            // name shown when not identified
            alias: new fields.StringField({ default: '' }),
            // actions within actionGroups
            actionGroups: new fields.ArrayField(new fields.ObjectField(), { required: true }),
        };
    }
}

/**
 * Default Actor data
 */
export default class ARSActorData extends actorDataSchema {
    /** @inheritdoc */
    static migrateData(source) {
        // console.log('ARSActorData migrateData', { source });
    }

    /** @inheritDoc */
    static defineSchema() {
        return foundry.utils.mergeObject(super.defineSchema(), {});
    }
}
