import { actorDataSchema } from './actor.js';
const fields = foundry.data.fields;

/**
 * Schema for Lootables
 */
export default class ARSActorMerchantData extends actorDataSchema {
    /** @inheritdoc */
    static migrateData(source) {
        super.migrateData(source);
        // console.log('ARSActorLootable migrateData', { source }, source.attributes.movement.value);
    }

    /** @inheritDoc */
    static defineSchema() {
        return foundry.utils.mergeObject(super.defineSchema(), {
            markup: new fields.SchemaField({
                buy: new fields.NumberField({ integer: false, require: true, initial: 1.0, default: 1.0 }),
                sell: new fields.NumberField({ integer: false, require: true, initial: 1.0, default: 1.0 }),
            }),
        });
    }
}
