import { actorDataSchema } from './actor.js';
const fields = foundry.data.fields;

/**
 * Schema for Lootables
 */
export default class ARSActorLootable extends actorDataSchema {
    /** @inheritdoc */
    static migrateData(source) {
        super.migrateData(source);
        // console.log('ARSActorLootable migrateData', { source }, source.attributes.movement.value);
    }

    /** @inheritDoc */
    static defineSchema() {
        return foundry.utils.mergeObject(super.defineSchema(), {
            lootable: new fields.BooleanField({ default: false }),
            opened: new fields.StringField({ default: '' }),
        });
    }
}
