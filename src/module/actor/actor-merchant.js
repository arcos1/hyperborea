export class ARSActorMerchant extends ARSActor {
    //TODO: make sell/buy functional

    /**
     * Set the buy markup.
     */
    setBuyMarkup(markup) {
        this.system.markup.buy = markup;
    }

    /**
     * Set the sell markup.
     */
    setSellMarkup(markup) {
        this.system.markup.sell = markup;
    }

    /**
     * Buy an item from the merchant.
     */
    buyItem(itemId) {
        const item = this.getItem(itemId);
        if (item) {
            const cost = item.system.cost * this.system.markup.buy;
            return { item, cost };
        }
        return null;
    }

    /**
     * Sell an item to the merchant.
     */
    sellItem(item) {
        const price = item.system.cost * this.system.markup.sell;
        this.createEmbeddedDocuments('Item', [item]);
        return price;
    }
}
