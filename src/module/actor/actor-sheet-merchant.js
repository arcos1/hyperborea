// import { ARSActorSheet } from './actor/actor-sheet.js';
// import { ARSNPCSheet } from './actor-sheet-npc.js';
export class ARSMerchantSheet extends ActorSheet {
    //TODO: prototype work for merchant

    /**
     * Specify the path to the HTML template.
     */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ['ars', 'sheet', 'actor', 'merchant'],
            template: 'systems/ars/templates/actor/merchant-sheet.hbs',
            width: 600,
            height: 400,
            actor: this.actor,
            secrets: [],
            // tabs: [{ navSelector: '.sheet-tabs', contentSelector: '.sheet-body', initial: 'description' }],
        });
    }

    /**
     * Get the data for the Merchant Actor Sheet.
     */
    async getData() {
        const context = super.getData();
        context.isGM = game.user.isGM;
        context.isDM = game.user.isDM;
        context.actor = this.actor;
        context.items = this.actor.items;
        context.playerItems = []; // populate this based on the context
        return context;
    }

    /**
     * Activate event listeners for the Merchant Actor Sheet.
     */
    activateListeners(html) {
        console.log('merchant activateListeners', { html });
        super.activateListeners(html);
        // Add additional listeners for Merchant specific actions
        html.find('.buy-item-button').click((event) => this._onBuyItemButtonClick.bind(this)(event));
        html.find('.sell-item-button').click((event) => this._onSellItemButtonClick.bind(this)(event));
        html.find('.set-buy-markup').change((event) => this._onSetBuyMarkup.bind(this)(event));
        html.find('.set-sell-markup').change((event) => this._onSetSellMarkup.bind(this)(event));
    }

    /**
     * Handle buy item button clicks.
     */
    _onBuyItemButtonClick(event) {
        console.log('_onBuyItemButtonClick', { event });
        event.preventDefault();
        // Handle the buy item button click
        const element = event.currentTarget;
        const itemId = element.dataset.itemId;
        const item = this.actor.getOwnedItem(itemId);
        const price = this.actor.sellItem(item);
        console.log(`Sold item: ${item.name} for ${price}`);
    }

    /**
     * Handle sell item button clicks.
     */
    _onSellItemButtonClick(event) {
        console.log('_onSellItemButtonClick', { event });
        event.preventDefault();
        // Handle the sell item button click
        const element = event.currentTarget;
        const itemId = element.dataset.itemId;
        // get item details from actor selling
        // const item = fromUuid(itemUuid);
        if (item) {
            const result = this.actor.buyItem(item);
            if (result) {
                console.log(`Bought item: ${result.item.name} for ${result.cost}`);
            }
        }
    }
}
